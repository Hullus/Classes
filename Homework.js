//Main class
class character {
    name = "";
    role = "";
    constructor(name, role) {
        this.name = name;
        this.role = role;
    }
    armor(armor){
        console.log(`${this.name} has  ${armor} armor points`);
    }
    damage(damage){
        if (this.role === "mage" || this.role === "support") {
            console.log(`${this.name} can deal ${damage} magic damage`);
        } else {
            console.log(`${this.name} can deal ${damage} attack damage`);
        }
    }
}

//role specific classes
class ADC extends character{
    constructor(name, role) {
        super(name, role);
    }
    range(range){
        if (range < 30){
            console.log(`${this.name} IS A CLOSE RANGE FIGHTER`);
        } else {
            console.log(`${this.name} IS A CLOSE RANGE FIGHTER`);
        }
    }
}


class mage extends character{
    magicPowers = [];
    constructor(name, role) {
        super(name, role);
    }
    fly(){
        console.log(`${this.name} CAN FLY`)
    }
    magicAbilities(Abilities){
        this.magicPowers.push(Abilities)
        console.log(`${this.name} has this power ${this.magicPowers}`)
    }
}


class support extends mage{
    constructor(name, role) {
        super(name, role);
    }
    heal(Heals){
        console.log(`${this.name} can heal ${Heals} points`)
    }

}
//execution

let Mage = new mage("Henry cavil","mage");

Mage.damage(1500);
Mage.armor(300);
Mage.fly();
Mage.magicAbilities("Summon frog");

console.log("-------------------------")

let Support = new support("lelouch vi britannia", "support")

Support.damage(500);
Support.armor(150);
Support.fly();
Support.magicAbilities("Command"); // Code geass-ს უყურეთ!!!!
Support.heal(30);

console.log("-------------------------");

let Adc = new ADC("Gela", "ADC");

Adc.damage(2000);
Adc.armor(250);
Adc.range(15);
